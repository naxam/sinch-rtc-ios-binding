using Foundation;
using System;
using UIKit;
using SinchSdk;
using System.IO;

namespace SinchCalling
{
    public enum ButtonBar
    {
        AnswerDecline,
        Hangup
    }

    public partial class CallViewController : SINUIViewController, ISINCallDelegate
    {
        NSTimer durationTimer;

        public CallViewController(IntPtr handle) : base(handle)
        {
        }

        ISINAudioController audioController
        {
            get
            {
                var appDelegate = (AppDelegate)UIApplication.SharedApplication.WeakDelegate;

                return appDelegate.Client.AudioController;
            }
        }

        ISINCall call;
        public ISINCall Call
        {
            get
            {
                return call;
            }

            set
            {
                call = value;
                call.WeakDelegate = this;
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (Call.Direction == SINCallDirection.Incoming)
            {
                lblCallState.Text = "";
                ShowButtons(ButtonBar.AnswerDecline);
                audioController.StartPlayingSoundFile(PathForSound("incoming.wav"), true);
            }
            else
            {
                lblCallState.Text = "calling...";
                ShowButtons(ButtonBar.Hangup);
            }

            btnAnswer.TouchUpInside += delegate
            {
                Accept();
			};
            btnDecline.TouchUpInside += delegate
			{
                Decline();
			};
            btnEndCall.TouchUpInside += delegate
			{
                Hangup();
			};
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            lblRemoteUserId.Text = Call.RemoteUserId;
        }

        //#pragma mark - Call Actions

        void Accept()
        {
            audioController.StopPlayingSoundFile();
            Call.Answer();
        }

        void Decline()
        {
			Call.Hangup();
			Dismiss();
        }

        void Hangup()
        {
			Call.Hangup();
			Dismiss();
        }

        void OnDurationTimer(NSTimer timer)
        {
            var duration = DateTime.Now - Call.Details.EstablishedTime.ToDateTime();
            SetDuration(duration);
        }

        //#pragma mark - SINCallDelegate
        [Export("callDidProgress:")]
        void CallDidProgress(ISINCall xcall)
        {
            lblCallState.Text = "ringing...";
            audioController.StartPlayingSoundFile("ringback.wav", true);
        }

        [Export("callDidEstablish:")]
        void CallDidEstablish(ISINCall xcall)
        {
            StartCallDurationTimer(OnDurationTimer);
            ShowButtons(ButtonBar.Hangup);
            audioController.StopPlayingSoundFile();
        }

        [Export("callDidEnd:")]
        void CallDidEnd(ISINCall xcall)
        {
            Dismiss();
            audioController.StopPlayingSoundFile();
            StopCallDurationTimer();
        }

        //#pragma mark - Sounds
        string PathForSound(string soundName)
        {
            return Path.Combine(NSBundle.MainBundle.ResourcePath, soundName);
        }

        //#pragma mark - Buttons
        void ShowButtons(ButtonBar buttons)
        {
            switch (buttons)
            {
                case ButtonBar.AnswerDecline:
                    btnAnswer.Hidden = false;
                    btnDecline.Hidden = false;
                    btnEndCall.Hidden = true;
                    break;
                case ButtonBar.Hangup:
                    btnEndCall.Hidden = false;
                    btnAnswer.Hidden = true;
                    btnDecline.Hidden = true;
                    break;
            }
        }

        //#pragma mark - Duration
        void SetDuration(TimeSpan span)
        {
            lblCallState.Text = string.Format("{0:D2}:{1:D2}", span.TotalSeconds / 60, span.TotalSeconds % 60);
        }

        void StartCallDurationTimer(Action<NSTimer> action)
        {
            durationTimer = NSTimer.CreateRepeatingScheduledTimer(0.5, action);
        }

        void StopCallDurationTimer()
        {
            durationTimer?.Invalidate();
            durationTimer = null;
        }
    }
}