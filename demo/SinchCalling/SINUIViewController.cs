﻿using System;
using UIKit;

namespace SinchCalling
{
    public class SINUIViewController : UIViewController
    {
        public SINUIViewController(IntPtr handle) : base(handle)
        {

        }

        public bool IsAppearing { get; set; }
        public bool IsDisappearing { get; set; }

        bool shouldDeferredDismiss;

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            if (View.Window == null)
            {
                IsAppearing = false;
                IsDisappearing = false;
            }
        }

        public override void ViewWillAppear(bool animated)
        {
            IsAppearing = true;
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            IsAppearing = false;
            DismissIfNecessary();
        }

        public override void ViewWillDisappear(bool animated)
        {
            IsDisappearing = true;
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            IsAppearing = false;
        }

        //#pragma mark - Dismissal
        public void Dismiss()
        {
            if (IsDisappearing)
            {
                return;
            }

            if (IsAppearing)
            {
                shouldDeferredDismiss = (true);
                return;
            }

            DismissViewController(true, null);
        }

        void DismissIfNecessary()
        {
            if (shouldDeferredDismiss)
            {
                shouldDeferredDismiss = false;

                InvokeOnMainThread(() =>
                {
                    Dismiss();
                });
            }
        }
    }
}
