﻿using System;
using Foundation;
using SinchSdk;
using UIKit;

namespace SinchCalling
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate, ISINClientDelegate
    {
        // class-level declarations

        public ISINClient Client { get; set; }

        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            if (launchOptions != null) {
				var localNoti = launchOptions[UIApplication.LaunchOptionsLocationKey];

				HandleLocalNotification((UILocalNotification)localNoti);
            }

            RequestUserNotificationPermission();

            NSNotificationCenter.DefaultCenter.AddObserver("UserDidLoginNotification", null, null, (obj) => { 
                InitSinchClientWithUserId(obj.UserInfo["userId"].ToString());
            });

            return true;
        }

        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            HandleLocalNotification(notification);
        }

        void RequestUserNotificationPermission()
        {
            var ok = UIApplication.SharedApplication.RespondsToSelector(new ObjCRuntime.Selector("registerUserNotificationSettings:"));

            if (!ok) return;

            var types = UIUserNotificationType.Alert | UIUserNotificationType.Sound;
            var settings = UIUserNotificationSettings.GetSettingsForTypes(types, null);

            UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
        }

        //#pragma mark -

        void InitSinchClientWithUserId(string userId)
        {
            if (Client == null) {
                Client = Sinch.ClientWithApplicationKey("4ace3566-6c40-4919-a125-a4c2f02960f8", "5465XdoOTkuqoiDciiPV4g==", "sandbox.sinch.com", userId);

                Client.WeakDelegate = this;

                Client.SetSupportCalling(true);
                Client.Start();
                Client.StartListeningOnActiveConnection();
            }
        }

        void HandleLocalNotification(UILocalNotification notification)
        {
            if (notification == null) return;

            ISINNotificationResult result = Client.RelayLocalNotification(notification);

            if (result.IsCall && result.CallResult.IsTimedOut) {
                var alert = new UIAlertView("Missed call", $"Missed call from {result.CallResult.RemoteUserId}", null, null, "ok", null);

                alert.Show();
            }
        }

        //#pragma mark - SINClientDelegate
        [Export("client:logMessage:area:severity:timestamp:")]
        void client(ISINClient client, string message, string area, SINLogSeverity severity, NSDate timestamp)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

		[Export("clientDidStart:")]
        public void ClientDidStart(ISINClient client)
		{
			System.Diagnostics.Debug.WriteLine($"Sinch client started successfully (version: {Sinch.Version})");
        }

        [Export("clientDidFail:error:")]
        public void ClientDidFail(ISINClient client, NSError error)
		{
			System.Diagnostics.Debug.WriteLine($"Sinch client error: {error.LocalizedDescription}");
        }
    }
}

