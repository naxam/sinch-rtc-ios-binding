﻿using System;
using Foundation;
using UIKit;

namespace SinchCalling
{
    [Register("PaddedTextField")]
    public class PaddedTextField : UITextField
    {
        public PaddedTextField(IntPtr handler) : base(handler)
        {
        }

        UIEdgeInsets PaddingInsets() {
            return new UIEdgeInsets(5, 10, 5, 10);
        }

        public override CoreGraphics.CGRect TextRect(CoreGraphics.CGRect forBounds)
        {
            return base.TextRect(PaddingInsets().InsetRect(forBounds));
        }

        public override CoreGraphics.CGRect EditingRect(CoreGraphics.CGRect forBounds)
        {
            return base.EditingRect(PaddingInsets().InsetRect(forBounds));
        }
    }
}