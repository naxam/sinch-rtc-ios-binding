﻿using System;
using Foundation;
using SinchSdk;
using UIKit;

namespace SinchCalling
{
    public class CallViewController : UIViewController, ISINCallDelegate
    {
        protected CallViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.


        }

        ISINAudioController audioController
        {
            get
            {
                var appDelegate = UIApplication.SharedApplication.WeakDelegate as AppDelegate;

                return appDelegate.Client.AudioController;
            }
        }

        public NSTimer DurationTimer { get; set; }

        ISINCall call;
        public ISINCall Call
        {
            get
            {
                return call;
            }

            set
            {
                call = value;
                call.WeakDelegate = this;
            }
        }

        //#pragma mark - UIViewController Cycle

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (Call.Direction == SINCallDirection.Incoming)
            {

            }

            //  if ([self.call direction] == SINCallDirectionIncoming) {

            //	[self setCallStatusText:@""];
            //	[self showButtons:kButtonsAnswerDecline];
            //	[[self audioController]
            //	startPlayingSoundFile:[self pathForSound:@"incoming.wav"]
            //	loop:YES];
            //} else {
            //  [self setCallStatusText:@"calling..."];
            //  [self showButtons:kButtonsHangup];
            //}
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            //self.remoteUsername.text = [self.call remoteUserId];
        }

        ////#pragma mark - Call Actions

        //- (IBAction) accept:(id) sender
        //{
        //	[[self audioController]
        //	stopPlayingSoundFile];
        //	[self.call answer];
        //}

        //- (IBAction) decline:(id) sender
        //{
        //	[self.call hangup];
        //	[self dismiss];
        //}

        //- (IBAction) hangup:(id) sender
        //{
        //	[self.call hangup];
        //	[self dismiss];
        //}

        //- (void) onDurationTimer:(NSTimer*) unused
        //{
        //	NSInteger duration = [[NSDate date]
        //	timeIntervalSinceDate:[[self.call details]
        //	establishedTime]];
        //	[self setDuration:duration];
        //}

        //#pragma mark - SINCallDelegate

        //- (void) callDidProgress:(id<SINCall>) call
        //{
        //	[self setCallStatusText:@"ringing..."];
        //	[[self audioController]
        //	startPlayingSoundFile:[self pathForSound:@"ringback.wav"]
        //	loop:YES];
        //}

        //- (void) callDidEstablish:(id<SINCall>) call
        //{
        //	[self startCallDurationTimerWithSelector:@selector(onDurationTimer:)];
        //	[self showButtons:kButtonsHangup];
        //	[[self audioController]
        //	stopPlayingSoundFile];
        //}

        //- (void) callDidEnd:(id<SINCall>) call
        //{
        //	[self dismiss];
        //	[[self audioController]
        //	stopPlayingSoundFile];
        //	[self stopCallDurationTimer];
        //}

        //#pragma mark - Sounds

        //- (NSString*) pathForSound:(NSString*) soundName
        //{
        //  return [[[NSBundle mainBundle]
        //	resourcePath] stringByAppendingPathComponent:soundName];
        //}

    }
}
