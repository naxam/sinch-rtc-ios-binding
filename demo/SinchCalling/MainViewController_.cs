using Foundation;
using System;
using UIKit;
using SinchSdk;

namespace SinchCalling
{
    public partial class MainViewController : UIViewController, ISINCallClientDelegate
    {
        public MainViewController(IntPtr handle) : base(handle)
        {
        }

        ISINClient Client
        {
            get
            {
                var appDelgate = (AppDelegate)UIApplication.SharedApplication.WeakDelegate;

                return appDelgate.Client;

            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            btnCall.TouchUpInside += delegate
            {
                Call();
            };
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            Client.CallClient.WeakDelegate = this;
        }

        void Call()
        {
            if (string.IsNullOrWhiteSpace(txtCallee.Text) || !Client.IsStarted)
            {
                return;
            }

            var xcall = Client.CallClient.CallUserWithId(txtCallee.Text);
            PerformSegue("callView", new NSObjectDto<ISINCall> {
                Data = xcall
            });
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            var xcallVc = (CallViewController)segue.DestinationViewController;
            var xcall = (NSObjectDto<ISINCall>)sender;
            xcallVc.Call = xcall.Data;
        }

        //#pragma mark - SINCallClientDelegate
        [Export("client:didReceiveIncomingCall:")]
        void ClientDidReceiveIncomingCall(ISINClient xclient, ISINCall xcall)
        {
            PerformSegue("callView", (NSObject)xcall);
        }

        [Export("client:localNotificationIncomingCall:")]
        SINLocalNotification ClientLocalNotificationIncomingCall(ISINClient xclient, ISINCall xcall)
        {
            var notification = new SINLocalNotification
            {
                AlertAction = "Answer",
                AlertBody = string.Format("Incoming call from {0}", xcall.RemoteUserId)
            };

            return notification;
        }
    }

    class NSObjectDto<T> : NSObject {
        public T Data { get; set; }
    }
}