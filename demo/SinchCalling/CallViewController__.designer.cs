// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SinchCalling
{
    [Register ("CallViewController")]
    partial class CallViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnAnswer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnDecline { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnEndCall { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblCallState { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblRemoteUserId { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnAnswer != null) {
                btnAnswer.Dispose ();
                btnAnswer = null;
            }

            if (btnDecline != null) {
                btnDecline.Dispose ();
                btnDecline = null;
            }

            if (btnEndCall != null) {
                btnEndCall.Dispose ();
                btnEndCall = null;
            }

            if (lblCallState != null) {
                lblCallState.Dispose ();
                lblCallState = null;
            }

            if (lblRemoteUserId != null) {
                lblRemoteUserId.Dispose ();
                lblRemoteUserId = null;
            }
        }
    }
}