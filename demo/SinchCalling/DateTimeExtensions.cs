﻿using System;
using Foundation;

namespace SinchCalling
{
    public static class DateTimeExtensions
    {
		public static DateTime ToDateTime(this NSDate date)
		{
			DateTime newDate = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));
			return newDate.AddSeconds(date.SecondsSinceReferenceDate);
		}

		public static NSDate ToNSDate(this DateTime date)
		{
			DateTime newDate = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));
			return NSDate.FromTimeIntervalSinceReferenceDate((date - newDate).TotalSeconds);
		}
    }
}
