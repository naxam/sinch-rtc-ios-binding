using Foundation;
using System;
using UIKit;

namespace SinchCalling
{
    public partial class LoginViewController : UIViewController
    {
        public LoginViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            txtUserId.BecomeFirstResponder();

            btnLogin.TouchUpInside += delegate
            {
                if (string.IsNullOrWhiteSpace(txtUserId.Text)) {
                    return;
                }

                var userInfo = new NSDictionary("userId", txtUserId.Text);
                NSNotificationCenter.DefaultCenter.PostNotificationName("UserDidLoginNotification", null, userInfo);

                PerformSegue("mainView", null);
            };
        }
    }
}